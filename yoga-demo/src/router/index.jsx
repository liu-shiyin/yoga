import React from 'react'
import { Routes, Route } from 'react-router-dom';
import Home from "../views/Home.jsx"
import TrainingCourse from "../views/TrainingCourse.jsx"
import Answer from "../views/Answer.jsx"
import My from "../views/My.jsx"
export default function index() {
  return (
    <Routes>
      <Route path='/home' element={<Home />}></Route>
      <Route path='/trainingcourse' element={<TrainingCourse />}></Route>
      <Route path='/answer' element={<Answer />}></Route>
      <Route path='/my' element={<My />}></Route>

    </Routes>
  )
}

