//引入axios
import axios from 'axios'
//引入vuex,便于存放公共地址，防止代码冗余

// const ips = {
//   development: store.state.ip,
// }
const Axios = axios.create({
  baseURL: 'http://127.0.0.1:7001',
  method: 'GET',
})

Axios.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

Axios.interceptors.response.use(
  (config) => {
    let { data } = config
    return data
  },
  (error) => {
    return Promise.reject(error)
  }
)
export default Axios
