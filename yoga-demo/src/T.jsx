import React, { useState, useEffect } from 'react';
import { TabBar, TabBarItem } from 'tdesign-mobile-react';
import { HomeIcon, UserIcon,AddCircleIcon,ChatIcon,LogoChromeIcon } from 'tdesign-icons-react';
import { NavLink,useLocation} from 'react-router-dom';
import Routers from "./router"
function TabBarBaseDemo(props) {
  const list = [
    { name: 'home', text: '首页', icon: <HomeIcon /> },
    { name: 'trainingcourse', text: '培训课程', icon: <LogoChromeIcon /> },
    { name: 'answer', text: '问答', icon: <AddCircleIcon /> },
    { name: 'my', text: '我的', icon: <UserIcon /> }
  ];
  const localation=useLocation()
  const pathName=localation.pathname.split('/')[1]
  const [value, setValue] = useState(pathName=='swiper'?'home':pathName);
  console.log(localation.pathname.split('/')[1])
  const change = (changeValue) => {
    setValue(changeValue);
  };

  useEffect(() => {
    console.log('当前值：', value);
  }, [value]);

  return (
    <div className="demo-tab-bar">     
      <Routers/>
      {
    <TabBar value={value} onChange={change}>
        {list.map((item, i) => (
          <TabBarItem key={item.name || i} icon={item.icon} value={item.name}>
            <NavLink to={item.name}>{item.text}</NavLink>
          </TabBarItem>
        ))}
      </TabBar>}
      
    </div>
  );
}

export default TabBarBaseDemo;
